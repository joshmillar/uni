\documentclass[11pt]{article}
\usepackage{amsmath, amssymb, amscd, amsthm, amsfonts}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{csquotes}
\usepackage[most]{tcolorbox}
\usepackage{venndiagram}
\usepackage{pgfplots}
\usepackage{tikz}
\usetikzlibrary{calc,positioning,shapes.multipart,shapes}
\usepackage{caption}
\usepackage{subcaption}

% color scheme for listings:
\usepackage{listings}
\usepackage{xcolor}
\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}


\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},   
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2,
    language=R
}

\lstset{style=mystyle}

\oddsidemargin 0pt
\evensidemargin 0pt
\marginparwidth 40pt
\marginparsep 10pt
\topmargin -20pt
\headsep 10pt
\textheight 8.7in
\textwidth 6.65in
\linespread{1.2}
\setlength\parindent{0pt}
\setlength\parskip{1em plus 0.1em minus 0.2em}




\title{ESC764 - Applied Statistics - Lecture notes}
\author{Joshua Millar}
\date{}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{conjecture}[theorem]{Conjecture}

\newcommand{\rr}{\mathbb{R}}

\newcommand{\al}{\alpha}
\DeclareMathOperator{\conv}{conv}
\DeclareMathOperator{\aff}{aff}

\begin{document}

\maketitle

\section{Descriptive statistics and probability distributions}
\subsection{Boolean probability rules}
Given an event A, the following rules apply:
\begin{itemize}\setlength\itemsep{0em}
  \item Rule 1: $0 \le P(A) \le 1$
  \item Rule 2: $P(A) + P(\bar{A}) = 1$
\end{itemize}
For independent binary events $A$ and $B$ illustrated below.

\begin{venndiagram2sets}[labelA={$P(A)$}, labelB={$P(B)$}, labelOnlyAB={$P(A\cap B)$}]%
\fillACapB
    \setpostvennhook
    {%
    	\draw (2.6,1.6) node{$ P(A\cap B) $};
        }%
    \end{venndiagram2sets}
    
The following rules are added:
\begin{itemize}\setlength\itemsep{0em}
  \item Rule 3: $P(A\cap B) = P(A)P(B)$
  \item Rule 4: $P(A\cup B) = P(A)+P(B) - P(A\cap B)$
\end{itemize}
\begin{tcolorbox}[breakable,title=Example]
A coin flip and a dice rolled.
\begin{itemize}\setlength\itemsep{0em}
	\item $P($heads $\cap$ `1'$) = \frac{1}{2}\times \frac{1}{6} = \frac{1}{12}$
	\item $P($heads $\cup$ `$>3$'$) = \frac{1}{2}+\frac{3}{6} - \left(\frac{1}{2}\times \frac{3}{6}\right) = \frac{3}{4}$
\end{itemize}	
\end{tcolorbox}
Conditional probability adds the following rules for non-independent events
\begin{itemize}\setlength\itemsep{0em}
  \item Rule 3': $P(A\cap B) = P(A)P(B|A)$
  \item Rule 2'': $P(A|B) + P(\bar{A}|B) = 1$; $P(A|\bar{B}) + P(\bar{A}|\bar{B}) = 1$
\end{itemize}

\subsection{Bayes theorem}
Bayes theorem allows a prior $P(A)$ to be revised based on new information to give the posterior $P(A|B)$:
\begin{align}
	P(A|B) &= \frac{P(A)P(B|A)}{P(B)} \\
	&=\frac{P(A)P(B|A)}{P(A)P(B|A)+P(\bar{A})P(B|\bar{A})}\nonumber
\end{align}
Which can be represented graphically below:

\begin{center}
\begin{tikzpicture}[scale=0.6]
\draw[black, thick] (-5, -5) rectangle (5, 5);
\filldraw[fill=cyan, draw=black] (-5, -5) rectangle (-4, 5);
\filldraw[fill=blue, draw=black] (-5, -5) rectangle (-4, -2);
\filldraw[fill=lightgray, draw=black] (-4, -4) rectangle (5, 5);
\filldraw[fill=gray, draw=black] (-4, -5) rectangle (5, -4);
\draw [decorate,decoration={brace,amplitude=10pt},xshift=-4pt,yshift=0pt]
(-5.5, -5) -- (-5.5,-2) node [black,midway,xshift=-1.1cm] 
{\footnotesize $P(B|A)$};
\draw [decorate,decoration={brace,amplitude=10pt,mirror,raise=4pt},yshift=0pt]
(5.5, -5) -- (5.5, -4) node [black,midway,xshift=1.2cm] {\footnotesize
$P(B|\bar{A})$};
\draw [decorate,decoration={brace,amplitude=10pt},xshift=-4pt,yshift=0pt]
(-5, 5.5) -- (-4,5.5) node [black,midway,yshift=0.6cm] 
{\footnotesize $P(A)$};
\draw [decorate,decoration={brace,amplitude=10pt},xshift=-4pt,yshift=0pt]
(-4, 5.5) -- (5,5.5) node [black,midway,yshift=0.6cm] 
{\footnotesize $P(\bar{A})$};
\end{tikzpicture}
\end{center}

\begin{tcolorbox}[breakable,title=Example]
A Covid test has a 99\% sensitivity (i.e. $P(T|V)$) and a 97\% specificity (i.e. $P(\bar{T}|\bar{V})$). Then what is the probability that someone has covid (V) given they test positive (T) assuming $0.05\%$ of the population have the disease (i.e. P(V))
\begin{align*}
	P(V|T) &= \frac{P(V)P(T|V)}{P(V)P(T|V)+P(\bar{V})P(T|\bar{V})}\\
	&= \frac{0.0005*0.99}{0.0005*0.99 + (1-0.0005)(1-0.97)}\\
	&= 0.0162
\end{align*}	
\end{tcolorbox}
\subsection{Binomial distribution}
Binomial is a discrete distribution which describes the outcome of $n$ trials with probability $p$: $\text{Bin}(n, p)$. The poisson distribution is a discrete distribution which describes the likelihood of a given number of events occurring within a fixed period, given the known mean rate of occurrence. If a line is drawn though these, they are approximated by the normal and exponential continuous distributions.
\section{Descriptive statistics}
A statistic is a measure of an attribute of a sample, which can be related to a parameter of the population which is a superset of the sample. For example, the ``sample mean'' (a statistic) of the sample is analogous to the ``population mean" (a parameter). 

\subsection{Central tendency}

\begin{align}
	\textbf{mean}(x) &= \overline x = \frac{1}{N} \sum_{i=1}^{N}x_i
\end{align}
Means are sensitive to extreme values, can use a trimmed mean (e.g. removing top n\% of values) but this removes information.

\begin{align}
	\textbf{median}(x) &= 
	\begin{cases}
		x_{(r+1)}, & \text{if } N = 2r+1\\
		\frac{1}{2}(x_{(r)} + x_{(r+1)}), & \text{if } N = 2r
	\end{cases}
\end{align}
Median can operate on ordinal data as well as numerical. It is robust to outliers however it does not reflect asymmetry. \textbf{mode}(x) is the most frequent value, data can be unimodal or multimodal. $\textbf{midrange}(x) = \frac{\max{(x)} - \min{(x)}}{2}$ is also skewed by extremes. 
\begin{figure}[hbt]
\centering
  \includegraphics[width=0.9\textwidth]{img/meanmedianmode.png}
\end{figure}

%\begin{figure}
%     \centering
%     \begin{subfigure}[b]{0.3\textwidth}
%         \centering
%         \includegraphics[width=\textwidth]{img/hist_small.png}
%     \end{subfigure}
%     \hfill
%     \begin{subfigure}[b]{0.3\textwidth}
%         \centering
%         \includegraphics[width=\textwidth]{img/hist_med.png}
%     \end{subfigure}
%     \hfill
%     \begin{subfigure}[b]{0.3\textwidth}
%         \centering
%         \includegraphics[width=\textwidth]{img/hist_large.png}
%     \end{subfigure}
%\end{figure}
\subsection{Dispersion}

\begin{align}
	\textbf{range}(x) &= \max{(x)} - \min{(x)} \nonumber \\
					  &= x_{(N)} - x_{(1)} \text{ for ordered datasets}
\end{align}
\textbf{quartiles} are an extension of the median, just split into four equal sets rather than two. The interquartile range $\textbf{IQR}(x) = x_{75\%} - x_{25\%}$. Variance is defined as:
\begin{align}
	\textbf{var}(x) = \sigma^2 = \frac{1}{N} \sum_{i=1}^{N} (x_i-\overline{x})^2
\end{align}
which converges to the population variance from above with increasing sample size. 

Unlike variance, absolute average deviation and median absolute deviation are less sensitive to extreme values:
\begin{align}
	\textbf{AAD}(x) = \frac{1}{N} \sum_{i=1}^{N} \left| x_i - \overline{x} \right|
\end{align}
\begin{align}
	\textbf{MAD}(x) = \text{median}\left( \{\left| x_1 - \overline{x} \right| , \left| x_2 - \overline{x} \right| ,..., \left| x_n - \overline{x} \right| \} \right)
\end{align}
\subsection{Higher-order statistics}
Skewness is a measure of the asymmetry of the data:
\begin{align}
	\gamma_1 &= \frac{1}{N \sigma^3} \sum_{i=1}^{N} (x_i-\overline{x})^3
\end{align}
Kurtosis is a measure of the ``peaked-ness'' or ``tailed-ness'' of the data:
\begin{align}
	\gamma_1 &= \frac{1}{N \sigma^3} \sum_{i=1}^{N} (x_i-\overline{x})^3
\end{align}
\section{Probability distributions}
In general there are three families of distributions. Their PDFs and some samples from parent distributions are displayed below. 
\subsection{Exponential distributions}
Exponential distributions have the general form $f(x, ...) = \alpha \exp (-x^\beta)$
Normal distribution is often used for real-valued random variables whose distributions are not known:
\begin{align}
	f(x, \mu, \sigma) = \frac{1}{\sqrt{2\pi \sigma^2}}\exp\left(\frac{-(x-\mu)^2}{2\sigma^2}\right)
\end{align}
\begin{lstlisting}
normal.large <- rnorm(1000, mean=0, sd=1)
hist(normal.large)
\end{lstlisting}

\begin{figure*}[h!]
\centering
	\includegraphics[width=0.5\textwidth]{img/hist_large.png}
\end{figure*}
The poisson distribution measures the probability of an event occurring over an interval, given the mean rate of occurrence:
\begin{align}
	f(x, \lambda) = \frac{\lambda^x}{x!}\exp(-\lambda)
\end{align}
\begin{lstlisting}
x.poisson <- rpois(1000, lambda=5)
hist(x.poisson)
plot(ecdf(x.poisson))
\end{lstlisting}
\begin{figure}[h!]
     \centering
     \begin{subfigure}[b]{0.4\textwidth}
         \centering
         \includegraphics[width=\textwidth]{img/hist_pois.png}
     \end{subfigure}
     \hfill
     \begin{subfigure}[b]{0.4\textwidth}
         \centering
         \includegraphics[width=\textwidth]{img/ecdf_pois.png}
     \end{subfigure}
\end{figure}

\subsection{Extreme value distributions}
Exponential distributions have limits on variance, real data often has some extreme values which are not outliers. It has the general form $f(x, ...) = \alpha \exp \left[ (-x/\lambda )^\beta\right]$, where $\alpha$ is the scale parameter and $\beta$ is the exponent. 

The gamma distribution is the sum of $\alpha$ exponential distributions and is used to model waiting times or death times:
\begin{align}
	f(x, \alpha, \beta) = \alpha\beta^\alpha x^{\alpha-1} \exp\left[-(x/\beta)^\alpha\right]
\end{align}
\begin{lstlisting}
x.gamma <- rgamma(1000, 0.83, 10.59)
hist(x.gamma)
\end{lstlisting}
\begin{figure*}[h!]
\centering
	\includegraphics[width=0.5\textwidth]{img/hist_gamma.png}
\end{figure*}
The Weibull distribution:
\begin{align}
	f(x, \lambda, k) = \frac{k}{\lambda}\left(\frac{x}{\lambda}\right)^{k-1}\exp\left[-\left(\frac{x}{\lambda}\right)^k\right]
\end{align}
\begin{lstlisting}
x.weib <- rweibull(1000, 2, 5.1)
hist(x.weib)
\end{lstlisting}
\begin{figure*}[h!]
\centering
	\includegraphics[width=0.5\textwidth]{img/hist_weib.png}
\end{figure*}
\subsection{Heavy-tailed distributions}
The pareto distribution is used in the description of social, scientific, geophysical and many other observable phenomena. It's an uneven distribution - 10\% of the objects have 90\% of the mass. 
\begin{align}
	f(x, \alpha, x_m) = \alpha x_m^\alpha x^{-\alpha-1}
\end{align}
where $\alpha$ is shape parameter and $x_m$ is the scale parameter. 
\begin{lstlisting}
x.pareto1 <- rpareto(100000, 10, 1)x.pareto1.5 <- rpareto(100000, 10, 1.5)x.pareto2 <- rpareto(100000, 10, 2)
plot(ecdf(x.pareto1), xlim=c(0, 1000))lines(ecdf(x.pareto1.5), col="green")lines(ecdf(x.pareto2), col="red", xlim=c(0, 200))
\end{lstlisting}
\begin{figure*}[h!]
\centering
	\includegraphics[width=0.5\textwidth]{img/pareto.png}
\end{figure*}
\section{Fitting probability distributions}
Fitting a distribution is the process of finding a mathematical function that represents the data. The process is iteritive:
\begin{enumerate}
	\item Find the distribution: either through domain expertise or by plotting the dataset against different classes of distributions
	\item Estimate the parameters: with e.g. MLE, moment estimation
	\item Evaluate the fit: either visually or with hypothesis testing
	\item If fit is good, stop; else return to 1
\end{enumerate}

\subsubsection{Finding the distribution}
Assuming no knowledge of the process being modelled, visual methods can be used to test which distributions are candidates to fit the data. 

Initially, a histogram of the dataset could be plotted.

\begin{figure}[h!]
\centering
  \includegraphics[width=0.5\textwidth]{img/hist_vtrim.png}
  \caption{Histogram of example dataset}
\end{figure}

The density plot is analgous to a smoothed histogram, which uses kernel density estimates to estimate a PDF of the dataset:

\begin{figure}[h!]
\centering
  \includegraphics[width=0.5\textwidth]{img/density_vtrim.png}
  \caption{Histogram of example dataset}
\end{figure}

From here, potential distributions can be compared to the dataset by examining a qq-plot. A perfect fit will result in a 45$^\circ$ line. In order to do this, you need to estimate the parameters (step 2). 

\begin{figure}[h!]
     \centering
     \begin{subfigure}[b]{0.25\textwidth}
         \centering
         \includegraphics[width=\textwidth]{img/qq_norm_vtrim.png}
         \subcaption{Dataset vs normal dist.}
     \end{subfigure}
     \begin{subfigure}[b]{0.25\textwidth}
         \centering
         \includegraphics[width=\textwidth]{img/qq_pois_vtrim.png}
         \subcaption{Dataset vs Poisson dist.}
         \end{subfigure}
         \begin{subfigure}[b]{0.25\textwidth}
         \centering
         \includegraphics[width=\textwidth]{img/qq_gamma_vtrim.png}
         \subcaption{Dataset vs gamma dist.}
         \end{subfigure}
         \caption{Qq plots of various distributions}
\end{figure}

In order to visualise the fit of the tail, a complimentary cumulative distrubution function can be plotted. This is a plot of the likelihood of a value being above a given value. When plotted on a log axis, the tails can be easily compared. In the example dataset, the normal distribution has a heavier tail than the dataset. 

\begin{figure}[h!]
\centering
  \includegraphics[width=0.5\textwidth]{img/ccdf_norm_vtrim.png}
  \caption{CCDF of the dataset (red) and normal distribution (black)}
\end{figure}

\subsubsection{Parameter estimation}
A set of realisations $\textbf{x}$  

\section{Time series analysis}
A time series differs from other types of data because the observations are dependent events - the goal of time series analysis is to model this dependency.
\subsection{Correlation, autocorrelation} 
The covariance can be used to show how pairs of variables are related:
\begin{align}
	cov(x, y) &= E\left[(x-\bar{x})(y-\bar(y)\right] \\ \nonumber
	& = \frac{1}{n-1}\sum_{i=1}^n (x_i -\bar{x})(y_i -\bar{y})
\end{align}
cov($x, y) > 0$ if both $x$ and $y$ are above or both below their mean value, and cov$(x, y) < 0 $ if $x$ and $y$ are opposite sides of their mean value. The correlation coefficient is a measure of the linear correlation between pairs of variables:
\begin{align}
	r &= \frac{cov(x, y)}{\sigma_x \sigma_y} \\ \nonumber
	&= \frac{\frac{1}{n-1}\sum_{i=1}^n (x_i -\bar{x})(y_i -\bar{y})}
	{\sqrt{\sum_i^n(x_i-\bar{x})^2}\sqrt{\sum_i^n(y_i-\bar{y})^2}}
\end{align}
\begin{itemize}
	\item $cor(x, y) \in [-1, 1]$
	\item $cor(x, y) \approx 1$: strong positive linear correlation
	\item $cor(x, y) \approx -1$: strong negative linear correlation
	\item else: mild or no correlation
\end{itemize}

Autocorrelation is the correlation of a process at different times:
\begin{align}
	R_{t, t+\tau}(x) = acf(x)&= \frac{E[(x_t-\mu)(x_{t+\tau}-\mu)]}{\sigma^2}
\end{align}
acf(x) $\in [-1, 1]$ and decreases with time lag (i.e. with increasing $t+\tau$). Three functions are plotted with their corresponding acf. For whitenoise this is very low (i.e. very little temporal dependency); for a linear increasing value, the acf decreases linearly; for a random walk function, the acf is hihgly temporally dependent and decreases slowly with time. 
\begin{figure}[htbp]
     \centering
     \begin{subfigure}[b]{0.7\textwidth}
         \centering
         \includegraphics[width=\textwidth]{img/timeseries_y.png}
     \end{subfigure}
     \hfill
     \begin{subfigure}[b]{0.7\textwidth}
         \centering
         \includegraphics[width=\textwidth]{img/timeseries_increase.png}
     \end{subfigure}
     \hfill
     \begin{subfigure}[b]{0.7\textwidth}
         \centering
         \includegraphics[width=\textwidth]{img/timeseries_rw.png}
     \end{subfigure}
\end{figure}

\newpage
\subsection{Stationarity}
A process is stationary if its statistical behaviour does not change over time:
\begin{itemize}
	\item Strict stationarity: distribution does not change with time. $F_x(x_{t_{1}}, ... , x_{t_n}) = F_x(x_{t_{1+\tau}}, ..., x_{t_{n+\tau}})$
	\item Weak stationarity: mean and variance are constant; covariance depends on time lag only
	\item Nonstationary: mean and variance is time-dependent
\end{itemize}
\subsection{Time series decomposition}
A time series can be split into three compoenents:
\begin{itemize}
	\item Trend: Underlying increase/decrease value in the series (e.g. linear, quadratic regression)
	\item Seasonailty: repeating cycle in the series ($x_t = x_{t-k}$ where k is period)
	\item Residual noise: Random variation in the seiries
\end{itemize}
These can be accounted for either with an additive model, $x(t) =$ trend + seasonality + residual; or a multiplicative model, $x(t) =$ trend $\times$ seasonality $\times$ residual. 
\begin{figure*}[htbp]
\centering
	\includegraphics[width=0.8\textwidth]{img/decompose.png}
\end{figure*}
If the decomposition is logical, then the residuals should be uncorrelated and close to a normal distribution. 
\subsection{Models}
There are a number of common approaches to modelling a univariate process:
Moving average model of order q, MA(q)
\begin{align}
	X_t = \mu + \epsilon_t + \sum_{i=1}^q \theta_i \epsilon_{t-1}
\end{align}
where $\theta_i$ are the parameters of the model, $\mu$ is the mean of $X_t$ (often $\mu =0$) and $\epsilon_t$ is the innovation. 

The autoregressive model of order $p$, AR(p):
\begin{align}
	X_t = c + \sum_{i=1}^p\phi_i X_{t-1} + \epsilon_t
\end{align}
where $\phi_i$ are the parameters and $c$ is a constant.
The ARMA model, ARMA(p,q) is a combination of MA(q) and AR(p) models
\begin{align}
	X_t = c + \epsilon_t + \sum_{i=1}^p\phi_i X_{t-1} + \sum_{i=1}^q \theta_i \epsilon_{t-1}
\end{align}

If a time series $y$ is not stationary, its first order difference $\Delta y = y_t - y_{t-1}$ often will be. The second-order difference of $y$ is $\Delta^2 y_t = \Delta y_t - \Delta y_{t-1}$. By using the difference of the variable, the ARMA model can be generalised to the ARIMA(p,d,q) model:
\begin{align}
	\Delta^d X_t = \epsilon_t + \sum_{i=1}^p\phi_i \Delta^d X_{t-1} + \sum_{i=1}^q \theta_i \epsilon_{t-1}
\end{align}

\subsection{General steps for time series forecasting}
\begin{enumerate}
	\item Check stationarity
	\item Difference: if nonstationary then take $d$ order difference until stationary
	\item Select AR, MA terms: Use acf to determine whether to use AR, MA or both.
	\item Build the model: Build model and forecast N steps
	\item Compare predicted values to the actual validation samples
\end{enumerate}
\end{document}


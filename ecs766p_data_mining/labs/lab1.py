import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler get_ipython().run_line_magic('matplotlib', 'inline')
#Import data
df = pd.read_csv('https://archive.ics.uci.edu/ml/machine-learning-databases/breast-ca df.columns = ['Sample code', 'Clump Thickness', 'Uniformity of Cell Size', 'Uniformit 'Marginal Adhesion', 'Single Epithelial Cell Size', 'Bare Nuclei', 'B
                'Normal Nucleoli', 'Mitoses','Class']
#Pre-process
df= df.drop(['Sample code'],axis=1)
df = df.replace('?',np.NaN)
df = df.fillna(df.median())
df['Bare Nuclei'] = pd.to_numeric(df['Bare Nuclei'])
#df.head()
col=df.columns features=col.tolist() feature=features[:-1] target=features[-1] X=df.loc[:,feature].values y=df.loc[:,target].values
#Standard Scaling
sc=StandardScaler() X=sc.fit_transform(X) pd.DataFrame(X,columns=feature).head()
#PCA
pca=PCA(n_components=2)
principalComponents=pca.fit_transform(X) principalDf=pd.DataFrame(data=principalComponents,columns=['principal component 1','p principalDf.head()
finalDf=pd.concat([principalDf,df[['Class']]],axis=1)
finalDf.head(100)
print(pca.explained_variance_ratio_)
plt.figure()
plt.figure(figsize=(10,10)) plt.xticks(fontsize=12)
plt.yticks(fontsize=14)
plt.xlabel('Principal Component 1',fontsize=20) plt.ylabel('Principal Component 2',fontsize=20) targets = [2, 4]
colors = ['r', 'g']
for target, color in zip(targets,colors):
indicesToKeep = finalDf['Class'] == target
plt.scatter(finalDf.loc[indicesToKeep, 'principal component 1'], finalDf.loc[indi
plt.legend(targets,prop={'size': 10})

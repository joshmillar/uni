%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Do not alter this block (unless you're familiar with LaTeX
\documentclass{article}
\usepackage[margin=1in]{geometry} 
\usepackage{amsmath,amsthm,amssymb,amsfonts, fancyhdr, color, comment, graphicx, environ}
\usepackage{xcolor}
\usepackage{mdframed}
\usepackage[shortlabels]{enumitem}
\usepackage{indentfirst}
\usepackage{hyperref}
% color scheme for listings:
\usepackage{listings}
\usepackage{xcolor}
\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}
\usepackage{pgfplots}
\usepackage{tikz}
\usetikzlibrary{calc,positioning,shapes.multipart,shapes}
\usepackage{subcaption}
\usepackage{caption}

\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},   
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2
}

\lstset{style=mystyle}

\setlength\parindent{0pt}
\setlength\parskip{1em plus 0.1em minus 0.2em}
\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,      
    urlcolor=blue,
}


\pagestyle{fancy}


\newenvironment{question}[2][Question]
    { \begin{mdframed}[backgroundcolor=gray!20] \textbf{#1 #2} \\}
    {  \end{mdframed}}

% Define solution environment
\newenvironment{answer}{\textbf{Answer}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Fill in the appropriate information below
\lhead{Joshua Millar 200351900}
\rhead{ECS766: Data Mining} 
\chead{\textbf{Assignment 3}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin{document}

\begin{question}{1.1}
	What is the advantage of using the Apriori algorithm in comparison with computing the support of every subset of an itemset in order to find the frequent itemsets in a transaction dataset?
\end{question}	
\begin{answer}
	For an item set of length $n$, there are $2^n -1$ subsets. It is computationally prohibitive to compute the support for this many itemsets. Apriori algorithm uses the property that all subsets of a frequent itemset must also be frequent to greatly reduce the number of candidate subsets which require the support to be computed. Therefore the advantage is that less computation is required to find frequent itemsets in the transaction dataset.	  
\end{answer}
\begin{question}{1.2}
Let  $L_1$  denote the set of frequent $1$-itemsets. For $k\ge2$, why must every frequent $k$-itemset be a superset of an itemset in $L_1$?
\end{question}
\begin{answer}
	The apriori property states that \textit{``all nonempty subsets of a frequent itemset must also be frequent"}. A corollary of this is that $L_k, k\ge 2$ are supersets of elements of $L_1$
\end{answer}
\begin{question}{1.3}
	Let  $L_2=\{\{1,2\},\{1,4\},\{2,3\},\{2,4\},\{3,5\}\}$. Compute the set of candidates $C_3$ that is obtained by joining every pair of joinable itemsets from $L_2$.
\end{question}
\begin{answer}
	$C_3$ is obtained by joining $L_2$ with itself where the members are joined iff their first element is the same. Therefore:
	\begin{align*}
		C_3 = L_2 \Join L_2 = \{\{1, 2, 4\}, \{2, 3, 4\}\}
	\end{align*}
\end{answer}
\begin{question}{1.4}
	Let $S_1$ denote the support of the association rule $\{\text{boarding pass}, \text{passport}\}\Rightarrow\{\text{flight}\}$. Let $S_2$ denote the support of the association rule $\{\text{boarding pass}\}\Rightarrow\{\text{flight}\}$. What is the relationship between $S_1$ and $S_2$? 
\end{question}
\begin{answer}
	Let $ A = \{\text{boarding pass}, \text{passport}\}$, $B=\{\text{boarding pass}\}$, $C=\{\text{flight}\}$:
	\begin{align*}
		S_1 &= S_{A\Rightarrow C} \equiv S_{A\cup C} \\
		S_2 &= S_{B\Rightarrow C} \equiv S_{B\cup C}.
	\end{align*}
	$B\subset A$, so $(B\cup C) \subset (A\cup C)$, and therefore through the apriori property: $S_{(B\cup C)} \ge S_{(A\cup C)}$
	\begin{align*}
		\therefore S_2 \ge S_1 .
	\end{align*}
\end{answer}
\begin{question}{1.5}
	What is the support of the rule $\{\}\Rightarrow \{Eggs\}$ in the transaction dataset used in Section 1 of this lab notebook?
\end{question}
\begin{answer} Let $A = \{\text{Eggs}\}$, 
\begin{align*}
	S_{\varnothing \Rightarrow A} &= S_{\varnothing \cup A}\\
	\varnothing \cup A &= A \\
	\therefore S_{\varnothing \cup A} &= S_{A} = \frac{N_A}{N} = \frac{4}{5} = 0.8
\end{align*}
\end{answer}
\begin{question}{1.6}
In the transaction dataset used in the tutorial presented above, what is the maximum length of a frequent itemset for a support threshold of 0.2?	
\end{question}
\begin{answer}
To achieve a support threshold of 0.2, the itemset needs to only appear once in the transaction dataset. Therefore the longest (unique) itemsets in the dataset will be the longest frequent itemsets:
\begin{align*}
	&\{\text{Onion, Nutmeg, Kidney Beans, Eggs, Yogurt, Milk}\}\\
	&\{\text{Onion, Nutmeg, Kidney Beans, Eggs, Yogurt, Dill}\}
\end{align*}
Both of which have a maximum length of 6. 
\end{answer}
\begin{question}{1.7}
Implement a function that computes the Kulczynski measure of two itemsets  $A$ and $B$. Use your function to compute the Kulczynski measure for itemsets $A=\{\text{Onion}\}$ and $B=\{\text{Kidney Beans, Eggs}\}$ in the transaction dataset used in this lab notebook. 
\end{question}
\begin{answer}
	
	\begin{lstlisting}[language=Python]
def calckulczynski(A, B, transaction_dataset):
  """
  computes kulczynski measure of A and B in dataset transaction_dataset. 
  ---
  A, B: set
  transaction_dataset: list
  """
  # first compute the confidence of A=>B
  def computeconfidence(A, B):
    NAunionB = 0
    NA = 0
    for itemset in transaction_dataset:
      if A.union(B).issubset(set(itemset)):
        NAunionB = NAunionB + 1
      if A.issubset(set(itemset)):
        NA = NA + 1
    confidence = NAunionB / NA
    
    return confidence

  confidenceAB = computeconfidence(A, B)
  confidenceBA = computeconfidence(B, A)

  # compute kulczynski measure per page 37 of week 8 notes
  kulczynskimeasure = (confidenceAB + confidenceBA) / 2

  return kulczynskimeasure

A = {'Onion'}
B = {'Kidney Beans', 'Eggs'}
kulczynskiAB = calckulczynski(A, B, dataset)
print("Kulczynski measure for A = {} and B = {} is {}".format(A, B, kulczynskiAB))
	\end{lstlisting}
	which outputs:
	\begin{lstlisting}[numbers=none]
Kulczynski measure for A = {'Onion'} and B = {'Eggs', 'Kidney Beans'} is 0.87
	\end{lstlisting}
\end{answer}
\begin{question}{1.8}
Implement a function that computes the imbalance ratio of two itemsets  $A$ and $B$. Use your function to compute the imbalance ratio for itemsets $A=\{\text{Onion}\}$ and $B=\{\text{Kidney Beans, Eggs}\}$  in the transaction dataset used in this lab notebook.	
\end{question}
\begin{answer}
\begin{lstlisting}[language=Python]
def calcimbalance(A, B, transaction_dataset):
  """
  computes imbalance ratio of A and B in dataset transaction_dataset. 
  ---
  A, B: set
  transaction_dataset: list
  """
  def calcsupportcount(A):
    # function to compute N for arbitrary set
    NA = 0
    for itemset in transaction_dataset:
      if A.issubset(set(itemset)):
        NA = NA + 1
    return NA 
  
  N_A = calcsupportcount(A)
  N_B = calcsupportcount(B)
  N_AunionB = calcsupportcount(A.union(B))

  # compute imbalance per page 38 of week 8 notes
  imbalance = abs(N_A - N_B) / (N_A + N_B - N_AunionB)
  return imbalance

A = {'Onion'}
B = {'Kidney Beans', 'Eggs'}
imbalanceAB = calcimbalance(A, B, dataset)
print("Imbalance ratio for A = {} and B = {} is {}".format(A, B, imbalanceAB))
\end{lstlisting}
which outputs:
\begin{lstlisting}[numbers=none]
Imbalance ratio for A = {'Onion'} and B = {'Eggs', 'Kidney Beans'} is 0.25
\end{lstlisting}
\end{answer}
\begin{question}{2.1}
	For an application on credit card fraud detection, we are interested in detecting contextual outliers. Suggest 2 possible contextual attributes and 2 possible behavioural attributes that could be used for this application, and explain why each of your suggested attribute should be considered as either contextual or behavioural.
\end{question}
\begin{answer}
	Examples of contextual attributes could be \textbf{time} and \textbf{location}; behavioural attributes could be \textbf{transaction amount} and \textbf{transaction category}. The behavioural attributes can be used to determine if the object is an outlier in the context defined by the contextual attributes. 
	
	For example, if there are high transaction amounts of the category ``pints of lager" at the time 6pm on a Friday at a location near to the customer's home, this may not be an outlier. However, if these same objects occurred at 9am on a Tuesday, far from the customer's home, this may be an outlier.  
\end{answer}

\begin{question}{2.2}
	Assume that you are provided with the University of Wisconsin breast cancer dataset from the Week 3 lab, and that you are asked to detect outliers from this dataset. Additional information on the dataset attributes can be found online. Explain one possible outlier detection method that you could apply for detecting outliers for this particular dataset, explain what is defined as an outlier for your suggested approach given this particular dataset, and justify why would you choose this particular method for outlier detection.
\end{question}
\begin{answer}
	A statistical method would be appropriate for this dataset. A simple parametric univariate outlier detection method could be used by defining a five-number summary for each attribute. For each attribute, the lower quartile (Q1), median (Q2) and upper quartile (Q3) are computed. Then a max and min value are defined as $Q3 + 1.5\times IQR$ and $Q1 - 1.5\times IQR$, respectively, where $IQR = Q3-Q1$. Any values above the max or below the min would be classified as outliers. This approach is appropriate because the attributes are likely to be distributed by a stochastic process on a population level. 
\end{answer}
\begin{question}{2.3}
	The monthly rainfall in the London borough of Tower Hamlets in 2019 had the following amount of precipitation (measured in mm, values from January-December 2018): \{22.93, 20.69, 25.75, 23.84, 25.34, 3.25, 23.55, 28.28, 23.72, 22.42, 26.83, 23.82\}. Assuming that the data is based on a normal distribution, identify outlier values in the above dataset using the maximum likelihood method.
\end{question}
\begin{answer}
	Using MLE, the mean, $\mu$ and standard deviation, $\sigma$ can be computed by
	\begin{align*}
		\mu & = \frac{1}{n}\sum_{i=1}^n x_i \approx 22.53 \\
		\sigma^2 &= \frac{1}{n} \sum_{i=1}^n (x - \mu)^2 \approx 37.58 \\
		\Rightarrow \sigma &\approx 6.13
	\end{align*}
	The value which deviates the most is 3.25mm which is $\frac{3.25 - 22.53}{6.13} = -3.15$ standard deviations away from the mean. Given the rule of 3$\sigma$ being an classed as an outlier, $3.25$ is the only outlier in the dataset. 
\end{answer}
\begin{question}{2.4}
	Using the stock prices dataset used in sections 1 and 2 of this lab notebook, estimate the outliers in the dataset using the one-class SVM classifier approach. As input to the classifier, use the percentage of changes in the daily closing price of each stock, as was done in section 1 of the notebook. Plot a 3D scatterplot of the dataset, where each object is color-coded according to whether it is an outlier or an inlier. Also compute a histogram and the frequencies of the estimated outlier and inlier labels. In terms of the plotted results, how does the one-class SVM approach for outlier detection differ from the parametric and proximity-based methods used in the lab notebook? What percentage of the dataset objects are classified as outliers?
\end{question}
\begin{answer}
	The code used to answer this question is listed below:
	\begin{lstlisting}[language=Python]
from matplotlib import colors

ee = OneClassSVM()
yhat = ee.fit_predict(delta)
# create a discrete cmap for the binary classification:
cmap = colors.ListedColormap(['blue', 'red'])
# Plot 3D scatterplot
fig = plt.figure(figsize=(10,6))
ax = fig.add_subplot(111, projection='3d')
p = ax.scatter(delta.MSFT, delta.F, delta.BAC,c=yhat,cmap=cmap)
ax.set_xlabel('Microsoft')
ax.set_ylabel('Ford')
ax.set_zlabel('Bank of America')
fig.colorbar(p, cmap=cmap, ticks=[-1, 1])
plt.show()

import seaborn as sns
sns.histplot(data=yhat, bins=2)

print("Frequency of outlier: {} / {}".format(len(yhat[yhat==-1]), len(yhat)))
print("Frequency of inlier: {} / {}".format(len(yhat[yhat==1]), len(yhat)))
	\end{lstlisting}
	
The resulting 3D scatter plot and histogram are shown below and the output for the frequencies of outliers and inliers is:
\begin{lstlisting}[numbers=none]
Frequency of outlier: 1259 / 2517
Frequency of inlier: 1258 / 2517
\end{lstlisting}

\begin{figure}[h!]
     \centering
     \begin{subfigure}[b]{0.45\textwidth}
         \centering
         \includegraphics[width=\textwidth]{img/svm-outlier.png}
     \end{subfigure}
     \hfill
     \begin{subfigure}[b]{0.45\textwidth}
         \centering
         \includegraphics[width=\textwidth]{img/svm-hist.png}
     \end{subfigure}
     \hfill
\end{figure}

I.e. almost exactly 50\% of the dataset is classified as an outlier. The results differ in that the results from the SVM classifier are binary (either outlier or inlier), whereas the plots for the proximity-based methods are coloured by continuous outlier scores and require further domain knowledge to convert these scores into an outlier classification. 
	
	
\end{answer}

\begin{question}{2.5}
	This question will combine concepts from both data preprocessing and outlier detection. Using the house prices dataset from Section 3 of this lab notebook, perform dimensionality reduction on the dataset using PCA with 2 principal components (make sure that the dataset is z-score normalised beforehand, and remember that PCA should only be applied on the input attributes). Then, perform outlier detection on the pre-processed dataset using the k-nearest neighbours approach using k=2. Display a scatterplot of the two principal components, where each object is colour-coded according to the computed outlier score.
\end{question}
\begin{answer}
	The code used to answer this question is listed below:
	\begin{lstlisting}[language=Python]
import pandas as pd
from sklearn import preprocessing, decomposition
import matplotlib.pyplot as plt
import seaborn as sns

# normalise with z-score normalisation
scaler = preprocessing.StandardScaler()
scaler.fit(X)
X_norm = scaler.transform(X)
# obtain the first two PCs on the scaled dataset:
pca = decomposition.PCA(n_components=2)
pca.fit(X_norm)
pcpl_components=pca.transform(X_norm)

# implement the knn on PC1, PC2 and compute outlier score
knn = 2
nbrs = NearestNeighbors(n_neighbors=knn, metric=distance.euclidean).fit(pcpl_components)
distances, indices = nbrs.kneighbors(pcpl_components)
outlier_score = distances[:,knn-1]

# plot pc1 against pc2
fig = plt.figure(figsize=(10,6))
ax = fig.add_subplot(111)
p = ax.scatter(
    pcpl_components[:, 0],
    pcpl_components[:, 1], 
    c=outlier_score,
    cmap='jet')
ax.set_xlabel('PC1')
ax.set_ylabel('PC2')
fig.colorbar(p)
plt.show()
	\end{lstlisting}
\end{answer}
The scatterplot is shown below:
\begin{figure*}[h!]
	\centering
	\includegraphics[width=0.8\textwidth]{img/pca-outlier.png}
\end{figure*}

\end{document}
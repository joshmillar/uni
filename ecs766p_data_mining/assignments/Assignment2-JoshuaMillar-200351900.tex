%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Do not alter this block (unless you're familiar with LaTeX
\documentclass{article}
\usepackage[margin=1in]{geometry} 
\usepackage{amsmath,amsthm,amssymb,amsfonts, fancyhdr, color, comment, graphicx, environ}
\usepackage{xcolor}
\usepackage{mdframed}
\usepackage[shortlabels]{enumitem}
\usepackage{indentfirst}
\usepackage{hyperref}
% color scheme for listings:
\usepackage{listings}
\usepackage{xcolor}
\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}
\usepackage{pgfplots}
\usepackage{tikz}
\usetikzlibrary{calc,positioning,shapes.multipart,shapes}

\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},   
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2
}

\lstset{style=mystyle}

\setlength\parindent{0pt}
\setlength\parskip{1em plus 0.1em minus 0.2em}
\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,      
    urlcolor=blue,
}


\pagestyle{fancy}


\newenvironment{question}[2][Question]
    { \begin{mdframed}[backgroundcolor=gray!20] \textbf{#1 #2} \\}
    {  \end{mdframed}}

% Define solution environment
\newenvironment{answer}{\textbf{Answer}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Fill in the appropriate information below
\lhead{Joshua Millar 200351900}
\rhead{ECS766: Data Mining} 
\chead{\textbf{Assignment 2}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin{document}

\begin{question}{1.1}
A data warehouse for a music streaming company consists of the dimensions song, user, time (time and date of when the user listened to a song), and the two measures count (how many times a user listened to the song) and fee (fee paid by the streaming company to the artist every time a user listens to that song).
\begin{enumerate}
	\item Draw a schema diagram for the above data warehouse using a star schema.
	\item Starting with the base cuboid [time, user, song], what specific OLAP operations should be performed in order to list the total fee collected for a given song for a given month of a given year (e.g. October 2021)?
	\item Assume that the time dimension has 4 levels: day, month, quarter, year; and that the song and user dimensions both have 1 level (not including the virtual level 'all'). How many cuboids will this cube contain (including the base and apex cuboids)?
\end{enumerate}
\end{question}
\begin{answer}
	\textbf{1.1.1} A possible schema is illustrated below:

\tikzset{main/.style={
        draw,
        rectangle split,
        rectangle split parts=3,
        rectangle split part fill={black!20,white},
        minimum width=2.5cm,
        text width=2cm,
        align=left,
        font=\itshape
    },
    basic/.style={
        draw,
        rectangle split,
        rectangle split parts=2,
        rectangle split part fill={black!20,white},
        minimum width=2.5cm,
        text width=2cm,
        align=left,
        font=\itshape
    },
    Diamond/.style={ diamond, 
                        draw, 
                        shape aspect=2, 
                        inner sep = 2pt,
                        text centered,
                        fill=black!10!white,
                        font=\itshape
                      }}

\begin{center}
\begin{tikzpicture}
\node[main] (fact) {fact table
\nodepart{second}
song\_key\\
user\_key\\
time\_key\\
\nodepart{third}
count\\
fee};
\node[basic, right of=fact, xshift=3cm] (song) {dimension table
\nodepart{second}
\underline{song\_key}\\
artist\\
album\\
song\_name\\
song\_id};
\node[basic,left of=fact, xshift=-3cm] (user) {dimension table
\nodepart{second}
\underline{user\_key}\\
user\_name\\
user\_id
};
\node[basic,below of=fact, yshift=-2.5cm, xshift=-3cm] (time) {dimension table
\nodepart{second}
\underline{time\_key}\\
year\\
month\\
day\\
time\_of\_day};
\draw[-] ([yshift=+15pt]$(fact.east)$) -- ([yshift=1pt]$(song.west)$);
\draw[-] ([yshift=5pt]$(fact.west)$) -- ([yshift=1pt]$(user.east)$);
\draw[-] ([yshift=-5pt]$(fact.west)$) -- ([yshift=1pt]$(time.east)$);
\end{tikzpicture}
\end{center}
\textbf{1.1.2} Let the song key for the song of interest have the id `song\_id' then possible operations could be:
\begin{enumerate}
	\item \textbf{slice} for \\(song = `song\_id') \\ and (time=`2021') 
	\item \textbf{drill-down} on time \\  (from year to months)
	\item \textbf{slice} for \\ (time = `October')
	\item \textbf{sum} fee
\end{enumerate}

\textbf{1.1.3} For an $n$-dimensional cube, the total number of cuboids, T is the product of each dimension's number of levels, $L$, plus the virtual level all. I.e.:
\begin{align*}
	T = \prod_{i=1}^n (L_i + 1)
\end{align*}
So for this cube:
\begin{align*}
	T = \underbrace{(4 + 1)}_{\text{time}} \times \underbrace{(1 + 1)}_{\text{song}} \times \underbrace{(1 + 1)}_{\text{user}} = 20
\end{align*}

\end{answer}

\begin{question}{1.2}
	Suppose that a car rental company has a data warehouse that holds record ID lists of vehicles in terms of brands (Audi, Ford, Mini) and store branches (Tower Hamlets, Newham, Hackney). Each record consists of a combination of vehicle brand and branch, and records for all combinations exist. We would like to index the OLAP data using bitmap indices. Write down the base table for record IDs, and the corresponding bitmap index table for vehicle brand.
\end{question}
\begin{answer}
	The base table is:
\begin{table}[hbt]
\begin{center}
  \begin{tabular}{l|l}
    Brand & Branch \\
    \hline
    Audi & Tower Hamlets \\
    Audi & Newham  \\
    Audi & Hackney \\
    Ford & Tower Hamlets \\
    Ford & Newham  \\
    Ford & Hackney \\
    Mini & Tower Hamlets \\
    Mini & Newham  \\
    Mini & Hackney \\
  \end{tabular}
  \end{center}
\end{table}

And the bitmap index table on brand is:
\begin{table}[hbt]
\begin{center}
  \begin{tabular}{l|c|c|c}
    Branch & Audi & Ford & Mini \\
    \hline
    Tower Hamlets & 1 & 0 & 0 \\
    Newham  & 1 & 0 & 0 \\
    Hackney & 1 & 0 & 0 \\
    Tower Hamlets & 0 & 1 & 0 \\
    Newham  & 0 & 1 & 0 \\
    Hackney & 0 & 1 & 0 \\
    Tower Hamlets & 0 & 0 & 1 \\
    Newham  & 0 & 0 & 1 \\
    Hackney & 0 & 0 & 1 \\
  \end{tabular}
  \end{center}
\end{table}
\end{answer}
\begin{question}{1.3}
	Using the same CSV file and data cube in the above lab tutorial, modify the ``tutorial\_model.json" file to include aggregate measures for the minimum and maximum amount in the data cube. Using these implemented aggregate measures, produce the values for the minimum and maximum amount in the data per year. Make sure to show your workings in the PDF report. You can read the Cubes package documentation for assistance in this task.
\end{question}
\begin{answer}
	The json file was modified to create a new json file, \verb|tutorial_model_mod.json| which is listed below. The min and max aggregates have been added in lines 40 $\to$ 49.
	\\
	
	\begin{lstlisting}
	{
    "dimensions": [
        {
         "name":"item",
         "levels": [
                {
                    "name":"category",
                    "label":"Category",
                    "attributes": ["category", "category_label"]
                },
                {
                    "name":"subcategory",
                    "label":"Sub-category",
                    "attributes": ["subcategory", "subcategory_label"]
                },
                {
                    "name":"line_item",
                    "label":"Line Item",
                    "attributes": ["line_item"]
                }
            ]
        },
        {"name":"year", "role": "time"}
    ],
    "cubes": [
        {
            "name": "ibrd_balance",
            "dimensions": ["item", "year"],
            "measures": [{"name":"amount", "label":"Amount"}],
            "aggregates": [
                    {
                        "name": "amount_sum",
                        "function": "sum",
                        "measure": "amount"
                    },
                    {
                        "name": "record_count",
                        "function": "count"
                    },
                    {
                        "name": "amount_min",
                        "function": "min",
                        "measure": "amount"
                    },
                    {
                        "name": "amount_max",
                        "function": "max",
                        "measure": "amount"
                    }
                ],
            "mappings": {
                          "item.line_item": "line_item",
                          "item.subcategory": "subcategory",
                          "item.subcategory_label": "subcategory_label",
                          "item.category": "category",
                          "item.category_label": "category_label"
                         },
            "info": {
                "min_date": "2010-01-01",
                "max_date": "2010-12-31"
            }
        }
    ]
}		
	\end{lstlisting}
	
These can be queried by modifying the code in the lab notebook to load in the new json file and displaying the new aggregates:
\begin{lstlisting}[language=Python]
from cubes import Workspace

workspace = Workspace()
workspace.register_default_store("sql", url="sqlite:///data.sqlite")

workspace.import_model("tutorial_model_mod.json")

cube = workspace.cube("ibrd_balance")

browser = workspace.browser(cube)

result = browser.aggregate()
display(result.summary["record_count"])
display(result.summary["amount_max"])
display(result.summary["amount_min"])
\end{lstlisting}
Which outputs:
\begin{lstlisting}[numbers=none]
62
128577
-3043
\end{lstlisting}
\end{answer}

\begin{question}{1.4}
	Using the CSV file ``country-income.csv" (found in the week 5 supplementary lab documents), perform the following:
	\begin{enumerate}
		\item Load the CSV file using Cubes, create a JSON file for the data cube model, and create a data cube for the data. Use as dimensions the region, age, and online shopper fields. Use as measure the income. Define aggregate functions in the data cube model for the total, average, minimum, and maximum income. In your PDF report, show the relevant scripts and files created.
		\item Using the created data cube and data cube model, produce aggregate results for: the whole data cube; results per region; results per online shopping activity; and results for all people aged between 40 and 50.
	\end{enumerate}
\end{question}
\begin{answer}
	\textbf{1.4.1} The json file \verb|income_model.json| and the code used to load the cube and display the total, average min and max income are listed below.
	\begin{lstlisting}
		{
    "dimensions": [
        {"name": "Region"},
        {"name": "Age"},
        {"name": "Online_Shopper"}
    ],
    "cubes": [
        {
            "name": "country_income",
            "dimensions": ["Region", "Age", "Online_Shopper"],
            "measures": [{"name": "Income"}],
            "aggregates": [
                    {
                        "name": "income_sum",
                        "function": "sum",
                        "measure": "Income"
                    },
                    {
                        "name": "income_min",
                        "function": "min",
                        "measure": "Income"
                    },
                    {
                        "name": "income_max",
                        "function": "max",
                        "measure": "Income"
                    },
                    {
                        "name": "income_count",
                        "function": "count",
                        "measure": "Income"
                    }
                ]
            }
        
        ]
    }
	\end{lstlisting}
	\begin{lstlisting}[language=Python, name=1.4]
from sqlalchemy import create_engine
from cubes.tutorial.sql import create_table_from_csv

engine_new = create_engine('sqlite:///data.sqlite')
create_table_from_csv(engine_new,
                      "country-income.csv",
                      table_name="country_income",
                      fields=[
                          ("Region", "string"),
                          ("Age", "integer"),
                          ("Income", "integer"),
                          ("Online_Shopper", "string")],
                      create_id=True
                     )

from cubes import Workspace

workspace_new = Workspace()
workspace_new.register_default_store("sql", url="sqlite:///data.sqlite")

workspace_new.import_model("income_model.json")

cube_income = workspace_new.cube("country_income")

browser_income = workspace_new.browser(cube_income)

result_income = browser_income.aggregate()
	\end{lstlisting}
\textbf{1.4.2} The python listing above is continued below, to show results for the whole cube, per region, per online shopping activity; and for people aged 40-50. 
	\begin{lstlisting}[language=Python, name=1.4]
print("For entire cube:")
print("Total income: {}".format(result_income.summary["income_sum"]))
print("Average income: {}".format(result_income.summary["income_sum"] 
                              / result_income.summary["income_count"]))
print("Minimum income: {}".format(result_income.summary["income_min"]))
print("Maximum income: {}".format(result_income.summary["income_max"]))

result_income = browser_income.aggregate(drilldown=['Region'])
for this_result in result_income:
  print("For {}:".format(this_result["Region"]))
  print("Total income: {}".format(this_result["income_sum"]))
  print("Average income: {}".format(this_result["income_sum"] 
                                / this_result["income_count"]))
  print("Minimum income: {}".format(this_result["income_min"]))
  print("Maximum income: {}".format(this_result["income_max"]))

result_income = browser_income.aggregate(drilldown=['Online_Shopper'])
for this_result in result_income:
  print("For online shopper - {}:".format(this_result["Online_Shopper"]))
  print("Total income: {}".format(this_result["income_sum"]))
  print("Average income: {}".format(this_result["income_sum"] 
                                / this_result["income_count"]))
  print("Minimum income: {}".format(this_result["income_min"]))
  print("Maximum income: {}".format(this_result["income_max"]))


import cubes as cubes

cuts = [cubes.RangeCut("Age", [40], [50])]
cell = cubes.Cell(cube_income, cuts)
result_income = browser_income.aggregate(cell)
print("For ages 40-50:")
print("Total income: {}".format(result_income.summary["income_sum"]))
print("Average income: {}".format(result_income.summary["income_sum"] 
                              / result_income.summary["income_count"]))
print("Minimum income: {}".format(result_income.summary["income_min"]))
print("Maximum income: {}".format(result_income.summary["income_max"]))
	\end{lstlisting}
	Which outputs the following:
	\begin{lstlisting}[numbers=none]
For entire cube:
Total income: 768200
Average income: 76820.0
Minimum income: 57600
Maximum income: 99600
For Brazil:
Total income: 193200
Average income: 64400.0
Minimum income: 57600
Maximum income: 73200
For India:
Total income: 331200
Average income: 82800.0
Minimum income: 69600
Maximum income: 94800
For USA:
Total income: 243800
Average income: 81266.66666666667
Minimum income: 64800
Maximum income: 99600
For online shopper - No:
Total income: 386400
Average income: 77280.0
Minimum income: 62400
Maximum income: 99600
For online shopper - Yes:
Total income: 381800
Average income: 76360.0
Minimum income: 57600
Maximum income: 94800
For ages 40-50:
Total income: 451400
Average income: 75233.33333333333
Minimum income: 62400
Maximum income: 86400
	\end{lstlisting}
\end{answer}


\begin{question}{2.1}
	Consider a dataset  $\mathcal{D}$ that contains only two observations  $x_1=(1,-1)$ and $x_2=(-1,1)$. Suppose that the class of the first observation is $y_1=0$  and that the class of the second observation is $y_2=1$. How would a 1-nearest neighbour classifier based on the Euclidean distance classify the observation  $x=(-2,3)$? What are the distances between this new observation and each observation in the dataset?
\end{question}
\begin{answer}
	The dataset can be plotted below. By inspection, it can be seen that $\mathbf{x}$ is nearest $\mathbf{x_2}$, so would be classified as $y=y_2=1$. 
\begin{center}

\begin{tikzpicture}
\begin{axis}[
    axis lines=middle,
    xmin=-4, xmax=4,
    ymin=-4, ymax=4,
    xtick=\empty, ytick=\empty,
    xlabel=$x$, ylabel=$y$
]
\draw [dashed] (axis cs:-2,3) circle [black, radius=223];
\addplot [only marks] table {
1 -1
}
node[above,pos=1] {$x1$};
\addplot [only marks] table {
-1 1
}
node[above,pos=1] {$x2$};
\addplot [only marks] table {
-2 3
}
node[above,pos=1] {$x$};
\end{axis}
\end{tikzpicture}	
\end{center}	
This can be formalised by computing the Euclidean distance between the pairs $(\mathbf{x}, \mathbf{x_1})$ and $(\mathbf{x}, \mathbf{x_2})$:
\begin{align*}
		d(\mathbf{x, x_1}) &= \sqrt{((-2) - 1)^2 + (3 - (-1))^2}\\
		&= 5 \nonumber \\
		d(\mathbf{x, x_2}) &= \sqrt{((-2) - (-1))^2 + (3 - 1)^2}\\
		&= \sqrt{5} \nonumber
\end{align*}
As $d(\mathbf{x, x_1}) > d(\mathbf{x, x_2})$, the new observation is closest to $\mathbf{x}_2$ and should be classified as $y=y_2=1$. 
\end{answer}
\begin{question}{2.2}
Consider a dataset $\mathcal{D}$ that only contains observations of two different classes. Explain why a $k$-nearest neighbour classifier does not need a tie-breaking policy when $k$ is odd. 
\end{question}
\begin{answer}
	An observation in this dataset will be classified based on the most common classification of the $k$ closest observations. When $k$ is odd, the length of the set of nearest neighbours will be equal to $k$ and so cannot be split into even-sized subsets. i.e. there can only be one most common value. 
\end{answer}
    \begin{question}{2.3}
Explain why a classifier that obtains an accuracy of  99.9\%  can be terrible for some datasets.
    \end{question}
    \begin{answer}
    	A dataset may have imbalanced classes - some classes may have a far higher frequency than others. For example, if a dataset $\mathcal{D}=(\mathbf{X}_1, y_1),..., (\mathbf{X}_n, y_n)$ has classes $y \in [1, 0]$ and there are 1,000,000 instances of $y=1$ and 1,000 instances of $y=0$, then a classifier: 
    	\begin{align*}
    		f(\mathbf{X}_i) = 1
    	\end{align*}
    	would achieve an accuracy of $\frac{999,000}{1,000,000} = 99.9\%$. However, it will obviously not correctly classify unseen true observations with $y=0$. 
    \end{answer}
 \begin{question}{2.4}
 	Consider a classifier tasked with predicting whether an observation belongs to class $y$ (positive class). Suppose that this classifier has precision  $1.0$  and recall  $0.1$  on a test dataset. If this classifier predicts that an observation does not belong to class $y$, should it be trusted? Should it be trusted if it predicts that the observation belongs to class $y$?
 \end{question}
 \begin{answer}
 	If the classifier predicts that the observation \textit{does not} belong to $y$, this prediction cannot be trusted. This is because the classifier's relatively poor recall means it is not good at detecting positives and classifies a relatively large number of false negatives. 
 	
 	However, if the classifier predicts that the observation \textit{does} belong to the class $y$, this prediction can be trusted. The classifier has perfect prediction and there are no false positives. 
 \end{answer}
\begin{question}{2.5}
	Based on the confusion matrix shown in this lab notebook, what is the pair of classes that is most confusing for the  1 -nearest neighbour classifier trained in the previous sections?
\end{question}
\begin{answer}
	The confusion matrix is reproduced below. The most confusing pair is the predicted value of $4$ and true value of $9$ because it has the highest value of all non-diagonal elements.
\begin{figure}[hbt]
  \begin{center}
  	  \includegraphics[scale=0.55]{img/confusion.png}
  \end{center}
\end{figure}
	
\end{answer}
\begin{question}{2.6}
	Train a support vector machine classifier using the same (subsampled) training dataset used in the previous sections and compute its accuracy on the corresponding test dataset. You can use the default hyperparameters for the class SVC from sklearn.svm. Show the code in the report.
\end{question}
\begin{answer}
	The SVM classifier can be trained using the following code. This assumes the training and test dataset has already been defined.
	\begin{lstlisting}[language=Python]
from sklearn.svm import SVC

svclas = SVC()
svclas.fit(X_train, y_train)
print('Test dataset accuracy: {0}.'.format(svclas.score(X_test, y_test)))
	\end{lstlisting}

Which outputs the accuracy on the test dataset of 0.9275:
\begin{lstlisting}[numbers=none]
Test dataset accuracy: 0.9275.
\end{lstlisting}
\end{answer}
\begin{question}{2.7}
	Using the same (subsampled) training dataset used in the previous sections, employ \verb|GridSearchCV| to find the best hyperparameter settings based on 5-fold cross-validation for a \verb|RandomForestClassifier|. Consider \verb|n_estimators| $\in \{50,100,200\}$ and \verb|max_features| $\in\{0.1,0.25\}$. Use the default values for the remaining hyperparameters. Compute the accuracy of the best model on the corresponding test dataset. Show the code in the report.
\end{question}
\begin{answer}
	The best hyperparameter settings can be found and a random forest classifier trained using the code below.
	\begin{lstlisting}[language=Python]
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV

parameters = {'n_estimators': [50, 100, 200],
              'max_features': [0.1, 0.25]}
rfc = RandomForestClassifier()
rfc_cv = GridSearchCV(rfc, parameters, cv=5)
rfc_cv.fit(X_train, y_train)

print("Accuracy of best hyperparameter settings "
      "on the test dataset: {}". format(rfc_cv.score(X_test, y_test)))
	\end{lstlisting}
\end{answer}
Which outputs the accuracy on the test model of 0.915:
\begin{lstlisting}[numbers=none]
	Accuracy of best hyperparameter settings on the test dataset: 0.915
\end{lstlisting}
	

\end{document}